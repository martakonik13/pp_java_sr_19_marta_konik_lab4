//********************************************************************
//  BMIGUI.java     
//
//  Wyznacza BMI w GUI.
//********************************************************************

import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;

import javax.swing.*;

public class BMIGUI
{
   private int WIDTH = 300;
   private int HEIGHT = 170;

   private JFrame frame;
   private JPanel panel;
   private JLabel wzrostLabel, wagaLabel, BMILabel, wynikLabel,infoLabel;
   private JTextField wzrost, waga;
   private JButton oblicz;

   //-----------------------------------------------------------------
   //  Ustawia GUI.
   //-----------------------------------------------------------------
   public BMIGUI()
   {
      frame = new JFrame ("Kalkulator BMI");
      frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);

      //tworzenie etykiety dla pol tekstowych wzrostu i wagi info
      wzrostLabel = new JLabel ("Twoj wzrost w metrach:");
      wagaLabel = new JLabel ("Twoja waga w kilokramach: ");
      infoLabel = new JLabel ("");
      //stworz etykiete "to jsest twoje BMI" 
      BMILabel = new JLabel ( "to jest twoje BMI");
      //stworz etykiete wynik dla wartosci BMI
      wynikLabel = new JLabel ("");
      
      // stworz JTextField dla wzrostu
      wzrost = new JTextField(20);
      // stworz JTextField dla wagi
      waga = new JTextField(20);
      // stworz przycisk, ktory po wcisnieciu policzy BMI
      oblicz = new JButton("Oblicz!");
      // stworz BMIListener, ktory bedzie nasluchiwal czy przycis zostal nacisniety 
      oblicz.addActionListener(new BMIListener());
      // ustawienia JPanel znajdujacego sie na JFrame 
      panel = new JPanel();
      panel.setPreferredSize (new Dimension(WIDTH, HEIGHT));
      panel.setBackground (Color.pink);

      //dodaj do panelu etykiete i pole tekstowe dla wzrostu
      panel.add(wzrostLabel);
      panel.add(wzrost);
      //dodaj do panelu etykiete i pole tekstowe dla wagi
      panel.add(wagaLabel);
      panel.add(waga);
      //dodaj do panelu przycisk
      panel.add(oblicz);
      //dodaj do panelu etykiete BMI
      panel.add( BMILabel);
      //dodaj do panelu etykiete dla wyniku
      panel.add(wynikLabel);
      panel.add(infoLabel);
      //dodaj panel do frame 
      frame.getContentPane().add (panel);
 
   }

   //-----------------------------------------------------------------
   //  Wyswietl frame aplikacji podstawowej
   //-----------------------------------------------------------------
   public void display()
   {
      frame.pack();
      frame.show();
   }

   //*****************************************************************
   //  Reprezentuje action listenera dla przycisku oblicz.
   //*****************************************************************
   private class BMIListener implements ActionListener
   {
      //--------------------------------------------------------------
      //  Wyznacz BMI po wcisnieciu przycisku
      //--------------------------------------------------------------
	   public void actionPerformed (ActionEvent event)
	   {
		   String wzrostText, wagaText, info, wynik,wynik2;
		   int wzrostVal, wagaVal;
		   double bmi,a;
		   DecimalFormat df=new DecimalFormat();
		   df.setMaximumFractionDigits(2);
		   
		   //pobierz tekst z pol tekstowych dla wagi i wzrostu
		   wagaVal= Integer.parseInt(waga.getText());
		   wzrostVal = Integer.parseInt(wzrost.getText());
		   //Wykorzystaj Integer.parseInt do konwersji tekstu na integer
	//	   System.out.println("wz:"+wzrostVal + ",wg:" + wagaVal);
		   //oblicz  bmi = waga / (wzrost)^2
		   a= wzrostVal*0.01;
		   bmi = wagaVal/(a*a);
		   //Dodaj wynik do etykiety dla wyniku.
		   wynik=Double.toString(bmi);
		   wynikLabel.setText("wynik:" + df.format(bmi));
	//	   System.out.println(bmi);
		   // Wykorzystaj Double.toString do konwersji double na string.
		   info = check(bmi);
		   infoLabel.setText(info);
	   }
   }
   public String check(double bmi)
   {
	   String result = "";

	   if(bmi<19)
		   result = "niedowaga";
	   else if (bmi <= 25 )
		   result = "waga prawidlowa";
	   else if (bmi<= 30 )
		   result = "nadwaga";
	   else
		   result = "otylosc";
	   
	   return result;
   }
   
  
}

